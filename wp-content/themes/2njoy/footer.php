<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Eveny
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<!-- FOOTER WIDGETS -->
				<?php if ( is_active_sidebar( 'footer-widget-1' ) ) { ?>
						<div class="col-lg-5 col-sm-6 widget-area">
							<?php dynamic_sidebar( 'footer-widget-1' ); ?>
						</div>
				<?php } ?>
				<?php if ( is_active_sidebar( 'footer-widget-2' ) ) { ?>
						<div class="col-lg-5 col-sm-6 widget-area">
							<?php dynamic_sidebar( 'footer-widget-2' ); ?>
						</div>
				<?php } ?>
				<?php if ( is_active_sidebar( 'footer-widget-3' ) ) { ?>
						<div class="col-lg-5 col-sm-6 widget-area">
							<?php dynamic_sidebar( 'footer-widget-3' ); ?>
						</div>
				<?php } ?>
				<?php if ( is_active_sidebar( 'footer-widget-4' ) ) { ?>
						<div class="col-lg-5 col-sm-6 widget-area">
							<?php dynamic_sidebar( 'footer-widget-4' ); ?>
						</div>
				<?php } ?>
			</div>
			<div class="site-info">
        &copy; <a class="purple" href="/">2njoy</a> <?php echo date("Y") ?>. <span style="float: right;"><a href="http://www.lundgrendesign.se/">Hemsida av Lundgren Design</a></span>
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->

	<a href="#" class="back-to-top">
		<i class="icon-top"></i>
	</a>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
